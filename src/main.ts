import { createApp } from 'vue'
import App from './App.vue'
const app = createApp(App);
import DatePicker from "ant-design-vue";
import "ant-design-vue/dist/antd.min.css";
app.use(DatePicker);
app.mount('#app')
