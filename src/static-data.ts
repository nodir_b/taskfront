// https://raw.githubusercontent.com/kenjebaev/regions/master/regions.json
import type { FormItemProps } from "@/components/FormGenerator.vue";

import  datas  from './datas.json'

let regions = datas?.regions
let districts = datas?.districts

const field: FormItemProps[] = [
    {
        placeholder: "",
        label: "To'liq ism sharif",
        sm: 24,
        xs: 24,
        type: "input",
        name: "fio",
    },
    {
        placeholder: "",
        label: "Tug'ilgan sana",
        sm: 12,
        xs: 24,
        type: "date",
        name: "birthdate",
    },
    {
        placeholder: "",
        label: "Ilmiy daraja",
        sm: 12,
        xs: 24,
        type: "select",
        name: "level_education",
        isLoading: false,
        options: [
            {
                id: 0,
                name: "Bakalavr",
            },
            {
                id: 1,
                name: "Magistr",
            },
        ]
    },
    {
        placeholder: "",
        label: "Tug'ilgan viloyat",
        sm: 12,
        xs: 24,
        type: "select",
        name: "region",
        isLoading: false,
        options: regions,
        isParent: true,
        nameChild: 'district',
        nameChildId: 'region_id',
        childOptions: districts
    },
    {
        placeholder: "",
        label: "Tug'ilgan tuman",
        sm: 12,
        xs: 24,
        type: "select",
        name: "district",
        isLoading: false,
        options: []
    },
    {
        placeholder: "",
        label: "Qo'shimcha izox",
        sm: 24,
        xs: 24,
        type: "textarea",
        name: "description",
    },
    {
        placeholder: "",
        label: "Resume",
        sm: 12,
        xs: 24,
        type: "files",
        multipleFile: true,
        name: "files",
    },

];



// console.log(datas?.regions, 'regions');
// console.log(datas?.districts, 'districts');

export {
    field
};